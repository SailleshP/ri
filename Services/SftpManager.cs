﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Renci.SshNet;
using WebInterfaces;

namespace Services
{
    public class SftpManager: ISFTP
    {
        public bool IsSFTPLoginFileCreationSuccessful(string host, string ftpPort, string ftpUsername, string ftpPassword, string folder)
        {
            try
            {
                var folders = string.IsNullOrEmpty(folder) ? new string[] { "" } : folder.Split('/');
                StringBuilder directoryMaker = new StringBuilder("");
                int defaultSFtpPort = 115;
                int port = (Convert.ToInt32(ftpPort) == 0) ? defaultSFtpPort : Convert.ToInt32(ftpPort);
                using (var ftpClient = new SftpClient(host, port, ftpUsername, ftpPassword))
                {
                    ftpClient.Connect();
                    for (int i = 0; i < folders.Length; i++)
                    {
                        var directories = ftpClient.ListDirectory(directoryMaker.ToString());
                        List<string> lstDirectory = new List<string>();
                        foreach (var directory in directories)
                        {
                            lstDirectory.Add(directory.Name);
                        }
                        if (lstDirectory.Where(x => x.ToString().ToLower() == folders[i].ToLower()).Any())
                        {
                            directoryMaker.Append(folders[i] + "/");

                            if (i == folders.Length - 1)
                            {
                                UploadFileAndDeleteFromSFTPServer(directoryMaker, ftpClient);
                            }

                        }
                        else
                        {
                            if (!string.IsNullOrEmpty(folder)) directoryMaker.Append(folders[i] + "/");
                            if (!string.IsNullOrEmpty(folder)) ftpClient.CreateDirectory(directoryMaker.ToString());
                            if (i == folders.Length - 1)
                            {
                                UploadFileAndDeleteFromSFTPServer(directoryMaker, ftpClient);
                            }
                        }
                    }
                    ftpClient.Disconnect();
                }
                return true;

            }

            catch (Exception EX)
            {
                throw;
            }
        }

        private void UploadFileAndDeleteFromSFTPServer(StringBuilder directoryMaker, SftpClient ftpClient)
        {
            if (!string.IsNullOrEmpty(directoryMaker.ToString())) ftpClient.ChangeDirectory(directoryMaker.ToString());
            using (Stream s = GenerateStreamFromString("TestFile"))
            {
                ftpClient.BufferSize = 4 * 1024;
                ftpClient.UploadFile(s, "Sample.txt");
            }
            ftpClient.Delete("Sample.txt");
        }

        private static Stream GenerateStreamFromString(string s)
        {
            MemoryStream stream = new MemoryStream();
            StreamWriter writer = new StreamWriter(stream);
            writer.Write(s);
            writer.Flush();
            stream.Position = 0;
            return stream;


        }
    }
}
