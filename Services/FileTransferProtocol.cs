﻿using System;
using System.Collections.Generic;
using System.Text;
using WebInterface;

namespace Services
{
  public abstract class FileTransferProtocol : IProtocol
    {
       public abstract bool IsFtpLoginFileCreationSuccessful(string host, string ftpPort, string ftpUsername, string ftpPassword, string folder);

    }
}
