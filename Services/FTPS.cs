﻿using FluentFTP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace Services
{
    public class FTPS : FileTransferProtocol
    {
        private FtpClient _client;
        private int _ftpReadTimeout = 0;

        public object ConfigurationManager { get; private set; }

        private void Connect(string Username, string Password, string HostName)
        {
            if (string.IsNullOrEmpty(Username)) throw new ArgumentNullException();
            if (string.IsNullOrEmpty(Password)) throw new ArgumentNullException();
            if (string.IsNullOrEmpty(HostName)) throw new ArgumentNullException();

            _client = new FtpClient(HostName);
            _client.Credentials = new NetworkCredential(Username, Password);
            _client.Connect();
        }

        private void CreateSetDirectory(string DirectoryName)
        {
            if (string.IsNullOrEmpty(DirectoryName)) throw new ArgumentNullException();
            _client.CreateDirectory(DirectoryName);
            _client.SetWorkingDirectory(DirectoryName);
        }

        private void Upload(byte[] data, string FileName)
        {
            _client.Upload(data, FileName);
        }


        public void CreateFile(string UserName, string Password, string HostName, string FileName, byte[] data, string Directory)
        {
            Connect(UserName, Password, HostName);
            if (!string.IsNullOrEmpty(Directory)) CreateSetDirectory(Directory);
            Upload(data, FileName);
            Disconnect();
        }

        public void DeleteFile(string FileName)
        {
            _client.DeleteFile(FileName);
        }

        public string ReadFile(string UserName, string Password, string HostName, string FileName, string Directory)
        {
            Connect(UserName, Password, HostName);
            string filePath = Directory + "/" + FileName;

            var _ftpStream = _client.OpenRead(filePath);
            using (var reader = new StreamReader(_ftpStream))
            {
                var Output = reader.ReadToEnd();
                Disconnect();
                return Output;

            }

        }

        private void Disconnect()
        {
            _client.Disconnect();
            _client.Dispose();
        }

        public override bool IsFtpLoginFileCreationSuccessful(string host, string ftpPort, string ftpUsername, string ftpPassword, string folder)
        {
            this.Connect(ftpUsername, ftpPassword, host);
            if (!string.IsNullOrEmpty(folder)) this.CreateSetDirectory(folder);
            byte[] bytes = Encoding.UTF8.GetBytes("Test Export");
            this.Upload(bytes, "sample.txt");
            this.DeleteFile("sample.txt");
            this.Disconnect();
            return true;
        }
    }
}
