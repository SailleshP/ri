﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace Services
{
    public class FTP : FileTransferProtocol
    {
        public override bool IsFtpLoginFileCreationSuccessful(string host,string ftpPort,  string ftpUsername, string ftpPassword, string folder)
        {
            var folders = string.IsNullOrEmpty(folder) ? new string[] { "" } : folder.Split('/');
            StringBuilder directoryMaker = new StringBuilder("/");
            for (int i = 0; i < folders.Length; i++)
            {

                if (i >= 1) directoryMaker.Append(folders[i - 1] + "/");
                host = host + directoryMaker.ToString();
                FtpWebRequest request;
                if (host.StartsWith("ftp") && !host.StartsWith("ftps"))
                {
                    request = (FtpWebRequest)WebRequest.Create(new Uri(host));
                }
                else
                {
                    host = "ftp://" + host;
                    request = (FtpWebRequest)WebRequest.Create(new Uri(host));
                }
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = new NetworkCredential(ftpUsername, ftpPassword);
                request.KeepAlive = false;
                var response = (FtpWebResponse)request.GetResponse();
                if (!(response.StatusCode == FtpStatusCode.CommandOK || response.StatusCode == FtpStatusCode.FileActionOK || response.StatusCode == FtpStatusCode.DataAlreadyOpen || response.StatusCode == FtpStatusCode.OpeningData)) return false;
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                string names = reader.ReadToEnd();
                var Directories = names.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (Directories.Where(x => x.ToString().ToLower() == folders[i].Trim().ToLower()).Any())
                {
                    if (i == folders.Length - 1)
                    {
                        CreateFile(request, folders[i], ftpUsername, ftpPassword);
                        directoryMaker.Append(folders[i]);
                        host = host + folders[i];
                        Delete(request, folder, host, ftpUsername, ftpPassword);

                    }

                }
                else
                {
                    if (!string.IsNullOrEmpty(folder)) CreateDirectory(request, folders[i], host, ftpUsername, ftpPassword);
                    if (i == folders.Length - 1)
                    {
                        CreateFile(request, folders[i], ftpUsername, ftpPassword);
                        if (!string.IsNullOrEmpty(folder)) directoryMaker.Append(folders[i]);
                        if (!string.IsNullOrEmpty(folder)) host = host + folders[i];
                        Delete(request, folder, host, ftpUsername, ftpPassword);

                    }
                }


            }

            return true;
        }

        private bool CreateFile(WebRequest request, string folder, string userName, string password)
        {
            try
            {
                int buffLength = 2048;
                byte[] buff = new byte[buffLength];
                byte[] bytes = Encoding.UTF8.GetBytes("Test Export");
                request = (FtpWebRequest)FtpWebRequest.Create(new Uri(request.RequestUri + "/" + folder + "/" + "Sample.txt"));
                request.Credentials = new NetworkCredential(userName, password);
                request.Method = WebRequestMethods.Ftp.UploadFile;
                request.ContentLength = bytes.Length;
                Stream requestStream = request.GetRequestStream();
                requestStream.Write(bytes, 0, bytes.Length);
                requestStream.Close();
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                return true;
            }
            catch (Exception)
            {
                throw;
            }

        }
        private bool CreateDirectory(WebRequest request, string folder, string Host, string userName, string password)
        {
            try
            {
                var getFolders = folder.Split('/');
                StringBuilder directoriesToBeCreated = new StringBuilder("/");
                for (int i = 0; i < getFolders.Length; i++)
                {
                    directoriesToBeCreated.Append(getFolders[i] + "/");
                    if (Host.StartsWith("ftp") && !Host.StartsWith("ftps"))
                    {
                        request = (FtpWebRequest)WebRequest.Create(new Uri(Host + directoriesToBeCreated.ToString()));
                    }
                    else
                    {
                        Host = "ftp://" + Host;
                        request = (FtpWebRequest)WebRequest.Create(new Uri(Host + directoriesToBeCreated.ToString()));
                    }
                    // request = WebRequest.Create(new Uri("ftp://" + Host + directoriesToBeCreated.ToString()));
                    request.Credentials = new NetworkCredential(userName, password);
                    request.Method = WebRequestMethods.Ftp.MakeDirectory;
                    FtpWebResponse responses = (FtpWebResponse)request.GetResponse();

                }
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private bool Delete(WebRequest request, string folder, string host, string userName, string password)
        {
            try
            {
                if (host.StartsWith("ftp") && !host.StartsWith("ftps"))
                {
                    request = (FtpWebRequest)WebRequest.Create(new Uri(host + "//" + "Sample.txt"));
                }
                else
                {
                    host = "ftp://" + host;
                    request = (FtpWebRequest)WebRequest.Create(new Uri(host + "//" + "Sample.txt"));
                }
                request.Method = WebRequestMethods.Ftp.DeleteFile;
                request.Credentials = new NetworkCredential(userName, password);
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }

        }
    }
}
