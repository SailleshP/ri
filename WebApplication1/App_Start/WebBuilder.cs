﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using Helper;
using Services;
using WebInterface;
using WebInterfaces;

namespace WebApplication1.App_Start
{
    public class WebBuilder
    {
        public static IContainer Container { get; set; }
        public static IContainer Build()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            //builder.RegisterType<FtpManager>().As<IFTP>();
            //builder.RegisterType<SftpManager>().As<ISFTP>();
            Uri endPointA = new Uri("http://localhost:58919/"); // this is the endpoint HttpClient will hit
            HttpClient httpClient = new HttpClient()
            {
                BaseAddress = endPointA,
            };

            ServicePointManager.FindServicePoint(endPointA).ConnectionLeaseTimeout = 60000; // sixty seconds


            builder.Register(ctx => new HttpClient() { BaseAddress = new Uri("https://api.ipify.org") })
                .Named<HttpClient>("ipify")
                .SingleInstance();

            builder.Register(ctx => new HttpClient() { BaseAddress = new Uri("https://api.postcodes.io") })
                .Named<HttpClient>("postcodes.io")
                .SingleInstance();

            //builder.Register(ctx => new HttpClient() { BaseAddress = new Uri("https://api.ipify.org") }) .SingleInstance();
            builder.RegisterType<HttpClient>().InstancePerLifetimeScope(); // note the singleton
            builder.RegisterType<FileTransferProtocol>().As<IProtocol>().InstancePerLifetimeScope();
            builder.RegisterType<FTP>().Named<IProtocol>("1").InstancePerLifetimeScope();
            builder.RegisterType<SFTP>().Named<IProtocol>("2").InstancePerLifetimeScope();
            builder.RegisterType<FTPS>().Named<IProtocol>("3").InstancePerLifetimeScope();
            Container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(Container));
            Registry.Container = Container;
            return Container;
        }
    }
}