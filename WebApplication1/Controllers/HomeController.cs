﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private readonly HttpClient _client;
        private readonly ILifetimeScope _container;
        public HomeController(ILifetimeScope container)
        {
            _container = container;
            _client= _container.ResolveNamed<HttpClient>("postcodes.io");


        }

        public ActionResult Index()
        {

            _client.GetStringAsync("");
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}