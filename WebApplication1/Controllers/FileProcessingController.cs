﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;
using WebInterface;
using WebInterfaces;

namespace WebApplication1.Controllers
{
    public class FileProcessingController : Controller
    {
        private readonly IFTP _ftpservice;
        private readonly ISFTP _sftpservice;
        private readonly ILifetimeScope _container;
        public FileProcessingController(ILifetimeScope container)
        {
            _container = container;

        }


        // GET: FileProcessing
        public ActionResult Index()
        {
            return View("FTPDetails",new FtpDetailsViewModels());
        }

        [HttpPost]
        public ActionResult UpdateSetting(FtpDetailsViewModels ftpDetails)
        {

            if (ModelState.IsValid)
            {
                try
                {


                    var reportSubject = _container.ResolveOptionalNamed<IProtocol>(ftpDetails.FtpType.ToString());


                   var status= reportSubject.IsFtpLoginFileCreationSuccessful(ftpDetails.FtpHost.Trim(),ftpDetails.FtpPort, ftpDetails.FtpUserName, ftpDetails.FtpPassword.Trim(), ftpDetails.Directory);
                    if(status)
                    {
                        saveDetails();
                    }else
                    {
                        ModelState.AddModelError("FTPDetail.FtpHost", "Invalid FTP Details"); ;
                    }
                

                }
                catch (Exception ex)
                {

                    ModelState.AddModelError("FTPDetail.FtpHost", ex.Message);

                }

                return View("FTPDetails", ftpDetails);
            }
            else
            {
                return View("FTPDetails", ftpDetails);
            }
            

            //save ftp details in db with the help of Repository
        }

        private void saveDetails()
        {

        }

    }
}