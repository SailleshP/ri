﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public enum EFtpTypes
    {
        FTP = 1,
        SFTP = 2
    }

    public class FtpDetailsViewModels
    {
        private string _userName;
        public string FtpUserName
        {
            get
            {
                return _userName;
            }
            set
            {
                _userName = value;
            }
        }

        private string _password;
        public string FtpPassword
        {
            get
            {
                return _password;
            }
            set
            {
                _password = value;
            }
        }


        private string _host;

        
        public string FtpHost
        {
            get
            {
                return _host;
            }
            set
            {
                _host = value;
            }
        }

        public string FtpPort { get; set; }

         public int FtpType { get; set; }

         public string Directory
        {
            get
            {
                if (!string.IsNullOrEmpty(_directory))
                {
                    return _directory.Replace(@"//", @"/");
                }
                return _directory;
            }

            set
            {
                _directory = value;
            }
        }
        private string _directory = "";
              public int ModeId { get; set; }


}
}