﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebInterface
{
    public interface IProtocol
    {
        bool IsFtpLoginFileCreationSuccessful(string host, string ftpPort, string ftpUsername, string ftpPassword, string folder);
    }
}
