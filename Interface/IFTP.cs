﻿using System;

namespace WebInterfaces
{
    public interface IFTP
    {
        bool IsFtpLoginFileCreationSuccessful(string host, string ftpUsername, string ftpPassword, string folder);
    }
}
