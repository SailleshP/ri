﻿using System;
using System.Collections.Generic;
using System.Text;

namespace WebInterfaces
{
    public interface ISFTP
    {
        bool IsSFTPLoginFileCreationSuccessful(string host, string ftpPort, string ftpUsername, string ftpPassword, string folder);
    }
}
